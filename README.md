# Ansible Role: ssh

[![pipeline status](https://gitlab.com/barlebenx/ansible-role-ssh/badges/master/pipeline.svg)](https://gitlab.com/barlebenx/ansible-role-ssh/-/commits/master)

Setup ssh service with sshd_config.

By default security requirements are apply.

## Requirements

- See meta for OS requirements


## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - ssh
```

## License

MIT / BSD
